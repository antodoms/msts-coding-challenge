Rails.application.routes.draw  do
  namespace :api do
    namespace :v1 do
      	resources :users do
      		resources :address
      		resources :phone
      	end
    end
  end
end