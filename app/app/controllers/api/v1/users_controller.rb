class Api::V1::UsersController < ApiController
	
	def index
		@users = User.all
	end

	def create
		@user = User.new(user_params)
		@errors = nil
		@status = @user.save
		if !@status
			@errors = @user.errors.as_json
		end
	end

	def show
		@user = User.find_by(:id => params[:id])
	end

	def update
		@user = User.find_by(:id => params[:id])
		@status = @user.update_attributes(user_params)
		@errors = nil
		if !@status
			@errors = @user.errors.as_json
		end
	end

	def destroy
		@user = User.find_by(:id => params[:id])

		#binding.pry
		@status = false
		@errors = nil
		if @user.present? && @user.destroy
			@status = true
		elsif @user.present?
			@status = false
			@errors = @user.errors.as_json
		else
			@errors = {:user => "not found"}
		end
	end

	def user_params
		params.require(:user).permit(:name, :email)
	end
end
