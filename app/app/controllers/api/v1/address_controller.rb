class Api::V1::AddressController < ApiController

	before_action :find_user
	
	def index
	end

	def create
		@address = Address.new(user_address_params)
		@address.user_id = @current_user.id
		@errors = nil
		@status = @address.save
		if !@status
			@errors = @address.errors.as_json
		end
	end

	def show
		@address = Address.find_by(:id => params[:id], :user => @current_user)
	end

	def update
		@address = Address.find_by(:id => params[:id], :user => @current_user)

		@status = @address.update_attributes(user_address_params)
		@errors = nil
		if !@status
			@errors = @address.errors.as_json
		end
	end

	def destroy
		@address = Address.find_by(:id => params[:id], :user => @current_user)

		#binding.pry
		@status = false
		@errors = nil
		if @address.present? && @address.destroy
			@status = true
		elsif @address.present?
			@status = false
			@errors = @address.errors.as_json
		else
			@errors = {:address => "not found"}
		end
	end

	private

	def find_user
		@current_user = User.find_by(:id => params[:user_id])
	end

	def user_address_params
		params.require(:address).permit(:address_type, :address_1, :address_2, :address_3, :suburb, :state, :postcode)
	end
end
