class Api::V1::PhoneController < ApiController

	before_action :find_user
	
	def index
	end

	def create
		@phone = Phone.new(user_phone_params)
		@phone.user_id = @current_user.id
		@errors = nil
		@status = @phone.save
		if !@status
			@errors = @phone.errors.as_json
		end
	end

	def show
		@phone = Phone.find_by(:id => params[:id], :user => @current_user)
	end

	def update
		@phone = Phone.find_by(:id => params[:id], :user => @current_user)

		@status = @phone.update_attributes(user_phone_params)
		@errors = nil
		if !@status
			@errors = @phone.errors.as_json
		end
	end

	def destroy
		@phone = Phone.find_by(:id => params[:id], :user => @current_user)

		#binding.pry
		@status = false
		@errors = nil
		if @phone.present? && @phone.destroy
			@status = true
		elsif @phone.present?
			@status = false
			@errors = @phone.errors.as_json
		else
			@errors = {:phone => "not found"}
		end
	end

	private

	def find_user
		@current_user = User.find_by(:id => params[:user_id])
	end

	def user_phone_params
		params.require(:phone).permit(:phone_type, :number, :active)
	end
end
