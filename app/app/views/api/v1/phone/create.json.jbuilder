if @status && @current_user
	json.status @status
	
	json.user_id @current_user.id
	json.phone do
		json.id @phone.try(:id)
		json.phone_type @phone.try(:phone_type)
		json.number @phone.try(:number)
		json.active @phone.try(:active)
	end
else
	json.status @status
	json.errors @errors
end
