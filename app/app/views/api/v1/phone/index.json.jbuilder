if @current_user
	json.status true

	json.user_id @current_user.id
	json.phone(@current_user.phones) do |phone|
		json.id phone.try(:id)
		json.phone_type phone.try(:phone_type)
		json.number phone.try(:number)
		json.active phone.try(:active)
	end

else
	json.status false
	json.errors "Something Went Wrong"
end
