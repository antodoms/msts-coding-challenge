if @users
	json.status true

	json.users do 
		json.array!(@users) do |user|
			json.id user.id
		  	json.name user.name
		  	json.email user.email
		end
	end
else
	json.status false
	json.errors "Something Went Wrong"
end
