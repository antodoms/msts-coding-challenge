if @status && @user
	json.status @status

	json.user do
		json.email @user.email
		json.name @user.name
	end
else
	json.status @status
	json.errors @errors
end
