if @user
	json.status true
	json.user do
		json.id @user.id
		json.email @user.email
		json.name @user.name
		
	end
else
	json.status false
	json.errors "User not found"
end
