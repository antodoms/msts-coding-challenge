if @status && @address
	json.status @status
	json.message "Address has been successfully destroyed"
	
else
	json.status @status
	json.errors @errors
end
