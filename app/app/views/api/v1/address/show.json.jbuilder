if @address
	json.status true

	json.user_id @current_user.id
	json.address do
		json.id @address.try(:id)
		json.address_type @address.try(:address_type)
		json.address_1 @address.try(:address_1)
		json.address_2 @address.try(:address_2)
		json.address_3 @address.try(:address_3)
		json.suburb @address.try(:suburb)
		json.state @address.try(:state)
		json.postcode @address.try(:postcode)
	end
else
	json.status false
	json.errors "Address not found"
end
