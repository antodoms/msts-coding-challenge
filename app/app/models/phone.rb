class Phone < ApplicationRecord
	belongs_to :user

	validates :number, presence: true
	validates :phone_type, presence: true
	
	validates_length_of :phone_type, :minimum => 3
	validates_length_of :number, :maximum => 12, :minimum => 8
end
