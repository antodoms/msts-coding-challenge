class Address < ApplicationRecord
	include ActionView::Helpers::SanitizeHelper

	belongs_to :user

	validates :address_1, presence: true
	validates :suburb, presence: true
	validates :state, presence: true
	validates :postcode, presence: true

	validates_length_of :address_1, :maximum => 50
	validates_length_of :address_2, :maximum => 50
	validates_length_of :address_3, :maximum => 50
	validates_length_of :suburb, :maximum => 50
	validates_length_of :state, :maximum => 3 , :minimum => 2
	validates_length_of :postcode, :maximum => 4, :minimum => 3

	# validate :validate_qas, if: :changed?

	before_validation :sanitise_model_data

	def sanitise_model_data
		self.address_1 = sanitize_content(self.address_1) if self.address_1.present?
		self.address_2 = sanitize_content(self.address_2) if self.address_2.present?
		self.address_3 = sanitize_content(self.address_3) if self.address_3.present?
		self.suburb = sanitize_content(self.suburb) if self.suburb.present?
		self.postcode = sanitize_content(self.postcode) if self.postcode.present?
		self.state = sanitize_content(self.state) if self.state.present?
	end

	def sanitize_content(data)
		strip_tags(data)
	end
end
