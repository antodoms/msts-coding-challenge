class User < ApplicationRecord

  has_many :address
  has_many :phones

  accepts_nested_attributes_for :address
  accepts_nested_attributes_for :phones

  validates :email, :presence => true, :uniqueness => true
end
