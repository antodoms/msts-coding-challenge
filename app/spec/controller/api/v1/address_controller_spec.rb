require "rails_helper"

describe Api::V1::AddressController, type: :controller do
  render_views
  fixtures :users
  fixtures :address

  before { @current_user = users(:user1) }

  describe "GET 'index' " do
    it "returns a successful 200 response" do
       get :index, format: :json, params: {:user_id => @current_user}
      expect(response).to be_success
    end

    it "returns status as true in json" do
      get :index, format: :json, params: {:user_id => @current_user}

      expect(response_body["status"]).to eq(true)
    end

    it "returns address array" do
      get :index, format: :json, params: {:user_id => @current_user}

      expect(response_body["address"]).to eq(@current_user.address
        .select(:id,:address_type,:address_1,:address_2, :address_3, :suburb, :state, :postcode).as_json)
    end
  end

  describe " GET 'show' page for an address" do
    before { @current_address = address(:address1) }
    it "returns an status false and 'Address not found' error if the Address for a particular id 
    doesn't exist for a user" do
      get :show, format: :json, params: {:user_id => @current_user, id: 999999}
      expect(response_body["status"]).to eq(false)
      expect(response_body["errors"]).to eq("Address not found")
    end

    it "returns status true when recieved current_address data correctly" do
      get :show, format: :json, params: {:user_id => @current_user, id: @current_address.id }
      expect(response_body["status"]).to eq(true)
    end

    it "returns current_user data correctly" do
      get :show, format: :json, params: {:user_id => @current_user, id: @current_address.id }
      #binding.pry
      expect(response_body["address"]["id"]).to eq(@current_address.id)
      expect(response_body["address"]["address_type"]).to eq(@current_address.address_type)
      expect(response_body["address"]["address_1"]).to eq(@current_address.address_1)
      expect(response_body["address"]["address_2"]).to eq(@current_address.address_2)
      expect(response_body["address"]["address_3"]).to eq(@current_address.address_3)
      expect(response_body["address"]["suburb"]).to eq(@current_address.suburb)
      expect(response_body["address"]["postcode"]).to eq(@current_address.postcode)
      expect(response_body["address"]["state"]).to eq(@current_address.state)
    end
  end

  describe " POST 'create' an address for an user in the database" do
    before { 
      @data1 = { id: 4, address_1: "300 street rd", address_2: "", address_3: "", 
        suburb: "melbourne", state: "vic", postcode: "3000", address_type: "others" } 
      @data2 = { id: 4, address_1: "550 street rd", address_2: "", address_3: "", 
        suburb: "", state: "", postcode: "" }
      @errors_list = {:suburb =>["can't be blank"], 
        :state =>["can't be blank", "is too short (minimum is 2 characters)"], 
        :postcode =>["can't be blank", "is too short (minimum is 3 characters)"]}
    }

    it "returns a status true and returns the newly created aaddress" do

      post :create, format: :json, params: {:user_id => @current_user, :address => @data1}
      expect(response_body["status"]).to eq(true)
      expect(response_body["address"]).to eq(@data1.as_json)
    end

    it "returns an status false and an error if required fields are not entered" do
      post :create, format: :json, params: {:user_id => @current_user,:address => @data2}

      expect(response_body["status"]).to eq(false)

      expect(response_body["errors"]).to eq(@errors_list.as_json)
    end
  end



  describe " POST 'update' address page updates an existing address in the database" do
      before { 
        @current_address = address(:address3)
        }

      it "when a user is updated it returns the new data as json" do
        updated_address = @current_address
        updated_address.address_1 = "300 MSTS ELB"
        updated_address.address_2 = "STU ST"
        post :update, format: :json, params: {:user_id => @current_user, address: updated_address.as_json, id: @current_address.id}
        expect(response_body["status"]).to eq(true)

        expect(response_body["address"]["address_1"]).to eq(@current_address.address_1)
        expect(response_body["address"]["address_2"]).to eq(@current_address.address_2)

      end
  end

  describe " PUTS 'update' address page updates an existing address in the database" do
      before { 
        @current_address = address(:address3)
        }

      it "when a user is updated it returns the new data as json" do
        updated_address = @current_address
        updated_address.address_1 = "300 MSTS ELB"
        updated_address.address_2 = "STU ST"
        put :update, format: :json, params: {:user_id => @current_user, address: updated_address.as_json, id: @current_address.id}
        expect(response_body["status"]).to eq(true)

        expect(response_body["address"]["address_1"]).to eq(@current_address.address_1)
        expect(response_body["address"]["address_2"]).to eq(@current_address.address_2)

      end
  end

  describe " DELETE 'destroy' existing address in the database" do
    before { 
      @current_address = address(:address3)
      @error_user_not_found = {:address =>"not found"}
      }

    it "the response should return a sucess when a address is deleted" do
      delete :destroy, format: :json, params: {:user_id => @current_user, id: @current_address.id}
      expect(response_body["status"]).to eq(true)
      expect(response_body["message"]).to eq("Address has been successfully destroyed")
    end

    it "the response should return a status false with an error when a address is deleted twice" do
      delete :destroy, format: :json, params: {:user_id => @current_user, id: @current_address.id}
      delete :destroy, format: :json, params: {:user_id => @current_user, id: @current_address.id}
      expect(response_body["status"]).to eq(false)
      expect(response_body["errors"]).to eq(@error_user_not_found.as_json)
    end

    it "the response should return a status false with an error 'user not found' when try to delete a unknown user" do
      delete :destroy, format: :json, params: {:user_id => @current_user, id: 99999}
      expect(response_body["status"]).to eq(false)
      expect(response_body["errors"]).to eq(@error_user_not_found.as_json)
    end
  end

  def response_body
    JSON.parse(response.body)
  end
end