require "rails_helper"

describe Api::V1::PhoneController, type: :controller do
	render_views
	fixtures :users
  fixtures :phone

  before { @current_user = users(:user1) }

  describe "GET 'index' " do
    it "returns a successful 200 response" do
       get :index, format: :json, params: {:user_id => @current_user}
      expect(response).to be_success
    end

    it "returns status as true in json" do
      get :index, format: :json, params: {:user_id => @current_user}

      expect(response_body["status"]).to eq(true)
    end

   	it "returns address array" do
      get :index, format: :json, params: {:user_id => @current_user}

      expect(response_body["phone"]).to eq(@current_user.phones
        .select(:id,:phone_type,:number,:active).as_json)
    end
	end

  describe " GET 'show' page for a phone" do
    before { @current_phone = phone(:phone1) }
    it "returns an status false and 'Phone not found' error if the Phone for a particular id 
    doesn't exist for a user" do
      get :show, format: :json, params: {:user_id => @current_user, id: 999999}
      expect(response_body["status"]).to eq(false)
      expect(response_body["errors"]).to eq("Phone not found")
    end

    it "returns status true when recieved current_phone data correctly" do
      get :show, format: :json, params: {:user_id => @current_user, id: @current_phone.id }
      expect(response_body["status"]).to eq(true)
    end

    it "returns current_phone data correctly" do
      get :show, format: :json, params: {:user_id => @current_user, id: @current_phone.id }
      #binding.pry
      expect(response_body["phone"]["id"]).to eq(@current_phone.id)
      expect(response_body["phone"]["phone_type"]).to eq(@current_phone.phone_type)
      expect(response_body["phone"]["number"]).to eq(@current_phone.number)
      expect(response_body["phone"]["active"]).to eq(@current_phone.active)
    end
  end

  describe " POST 'create' a phone for an user in the database" do
    before { 
      @data1 = { id: 4, number: "06776558788", active: true, phone_type: "mobile" } 
      @data2 = { id: 4, number: "", active: true, phone_type: "" } 
      @errors_list = {:number=>["can't be blank", "is too short (minimum is 8 characters)"],
          :phone_type=>["can't be blank", "is too short (minimum is 3 characters)"]}
        }

    it "returns a status true and returns the newly created phone" do

      post :create, format: :json, params: {:user_id => @current_user, :phone => @data1}
      expect(response_body["status"]).to eq(true)
      expect(response_body["phone"]).to eq(@data1.as_json)
    end

    it "returns an status false and an error if required fields are not entered" do
      post :create, format: :json, params: {:user_id => @current_user,:phone => @data2}

      expect(response_body["status"]).to eq(false)

      expect(response_body["errors"]).to eq(@errors_list.as_json)
    end
  end


  describe " POST 'update' phone page updates an existing phone in the database" do
    before { 
      @current_phone = phone(:phone1)
      }

    it "when a phone is updated it returns the new data as json" do
      updated_phone = @current_phone
      updated_phone.number = "067659556444"
      updated_phone.phone_type = "work"
      updated_phone.active = false

      post :update, format: :json, params: {:user_id => @current_user, phone: updated_phone.as_json, id: @current_phone.id}
      expect(response_body["status"]).to eq(true)

      expect(response_body["phone"]["number"]).to eq(@current_phone.number)
      expect(response_body["phone"]["phone_type"]).to eq(@current_phone.phone_type)
      expect(response_body["phone"]["active"]).to eq(@current_phone.active)

    end
  end

  describe " PUT 'update' phone page updates an existing phone in the database" do
    before { 
      @current_phone = phone(:phone1)
      }

    it "when a phone is updated it returns the new data as json" do
      updated_phone = @current_phone
      updated_phone.number = "067659556444"
      updated_phone.phone_type = "work"
      updated_phone.active = false

      put :update, format: :json, params: {:user_id => @current_user, phone: updated_phone.as_json, id: @current_phone.id}
      expect(response_body["status"]).to eq(true)

      expect(response_body["phone"]["number"]).to eq(@current_phone.number)
      expect(response_body["phone"]["phone_type"]).to eq(@current_phone.phone_type)
      expect(response_body["phone"]["active"]).to eq(@current_phone.active)

    end
  end

  describe " DELETE 'destroy' existing phone in the database" do
    before { 
      @current_phone = phone(:phone3)
      @error_phone_found = {:phone =>"not found"}
      }

    it "the response should return a sucess when a phone is deleted" do
      delete :destroy, format: :json, params: {:user_id => @current_user, id: @current_phone.id}
      expect(response_body["status"]).to eq(true)
      expect(response_body["message"]).to eq("Phone has been successfully destroyed")
    end

    it "the response should return a status false with an error when a phone is deleted twice" do
      delete :destroy, format: :json, params: {:user_id => @current_user, id: @current_phone.id}
      delete :destroy, format: :json, params: {:user_id => @current_user, id: @current_phone.id}
      expect(response_body["status"]).to eq(false)
      expect(response_body["errors"]).to eq(@error_phone_found.as_json)
    end

    it "the response should return a status false with an error 'phone not found' when try to 
      delete a unknown phone" do
      delete :destroy, format: :json, params: {:user_id => @current_user, id: 99999}
      expect(response_body["status"]).to eq(false)
      expect(response_body["errors"]).to eq(@error_phone_found.as_json)
    end
  end

	def response_body
		JSON.parse(response.body)
	end
end