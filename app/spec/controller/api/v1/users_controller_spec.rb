require "rails_helper"

describe Api::V1::UsersController, type: :controller do
	render_views
	fixtures :users

	describe "GET 'index' " do
	    it "returns a successful 200 response" do
	       get :index, format: :json
	      expect(response).to be_success
	    end

	    it "returns status as true in json" do
	      get :index, format: :json

	      expect(response_body["status"]).to eq(true)
	    end

	   	it "returns user array" do
	      get :index, format: :json
	      expect(response_body["users"]).to eq(User.select(:id,:email,:name).as_json)
	    end
  	end

  	describe " GET 'show' page for a user" do
  		before { @current_user = users(:user1) }
  		it "returns an status false and 'user not found' error if the user for a particular id doesnt exist" do
  			get :show, format: :json, params: { id:  9999999}
  			expect(response_body["status"]).to eq(false)
  			expect(response_body["errors"]).to eq("User not found")
  		end

  		it "returns status true when current_user data correctly" do
  			get :show, format: :json, params: { id: @current_user.id }
  			expect(response_body["status"]).to eq(true)
  		end

  		it "returns current_user data correctly" do
  			get :show, format: :json, params: { id: @current_user.id }
  			#binding.pry
  			expect(response_body["user"]["id"]).to eq(@current_user.id)
  			expect(response_body["user"]["name"]).to eq(@current_user.name)
  			expect(response_body["user"]["email"]).to eq(@current_user.email)
  		end
  	end


  	describe " POST 'create' page creates a user in the database" do
  		before { 
  			@request_body = {:name => "New User", :email => "newuser@test.com"} 
  			@error_user_exists = {:email =>["has already been taken"]}
  		}

  		it "returns an status true and returns the newly created user" do

  			post :create, format: :json, :params => {:user => @request_body}
  			expect(response_body["status"]).to eq(true)
  			expect(response_body["user"]).to eq(@request_body.as_json)
  		end

  		it "returns an status false and an error if we try to create a user that already exists" do

  			post :create, format: :json, :params => {:user => @request_body}
  			post :create, format: :json, :params => {:user => @request_body}
  			expect(response_body["status"]).to eq(false)

  			@errors = nil
  			expect(response_body["errors"]).to eq(@error_user_exists.as_json)
  		end

  	end

  	describe " POST 'update' page updates an existing user in the database" do
  		before { 
  			@current_user = users(:user1) 
  			@request_body = {:user => @current_user}
  			}

  		it "when a user is updated it returns the new data as json" do
  			updated_user = @current_user
  			updated_user.name = "New Name"
  			updated_user.email = "newemail@test.com"
  			post :update, format: :json, params: { user: updated_user.as_json, id: @current_user.id}
  			expect(response_body["status"]).to eq(true)

  			expect(response_body["user"]["id"]).to eq(@current_user.id)
  			expect(response_body["user"]["name"]).to eq(updated_user.name)
  			expect(response_body["user"]["email"]).to eq(updated_user.email)
  		end

  	end

  	describe " PUTS 'update' page updates an existing user in the database" do
  		before { 
  			@current_user = users(:user1) 
  			@request_body = {:user => @current_user}
  			}

  		it "when a user is updated it returns the new data as json" do
  			updated_user = @current_user
  			updated_user.name = "New Name"
  			updated_user.email = "newemail@test.com"
  			put :update, format: :json, params: { user: updated_user.as_json, id: @current_user.id}
  			expect(response_body["status"]).to eq(true)

  			expect(response_body["user"]["id"]).to eq(@current_user.id)
  			expect(response_body["user"]["name"]).to eq(updated_user.name)
  			expect(response_body["user"]["email"]).to eq(updated_user.email)
  		end

  	end

  	describe " DELETE 'destroy' existing user in the database" do
  		before { 
  			@current_user = users(:user1)
  			@error_user_not_found = {:user =>"not found"}
  			}

  		it "the response should return a sucess when a user is deleted" do
  			delete :destroy, format: :json, params: {id: @current_user.id}
  			expect(response_body["status"]).to eq(true)
  			expect(response_body["message"]).to eq("user successfully destroyed")
  		end

  		it "the response should return a status false with an error when a user is deleted twice" do
  			delete :destroy, format: :json, params: {id: @current_user.id}
  			delete :destroy, format: :json, params: {id: @current_user.id}
  			expect(response_body["status"]).to eq(false)
  			expect(response_body["errors"]).to eq(@error_user_not_found.as_json)
  		end

  		it "the response should return a status false with an error 'user not found' when try to delete a unknown user" do
  			delete :destroy, format: :json, params: {id: 99999}
  			expect(response_body["status"]).to eq(false)
  			expect(response_body["errors"]).to eq(@error_user_not_found.as_json)
  		end
  	end

	def response_body
		JSON.parse(response.body)
	end
end