require "rails_helper"

describe WelcomeController, type: :controller do
	render_views
	describe "GET 'index' " do
	    it "returns a successful 200 response" do
	       get :index, format: :json
	      expect(response).to be_success
	    end

	    it "validate the response text" do
          headers = { "ACCEPT" => "application/json" }
	      get :index, format: :json
	      expect(response_body["status"]).to eq("Active")
	    end
  	end

	def response_body
		JSON.parse(response.body)
	end
end