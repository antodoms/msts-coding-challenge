class CreateAddresses < ActiveRecord::Migration[5.1]
  def change
    create_table :addresses do |t|
      t.string :address_1
      t.string :address_2
      t.string :address_3
      t.string :suburb
      t.string :state
      t.string :postcode
      t.string :address_type
      t.references :user
      t.timestamps null: false
    end
  end
end
