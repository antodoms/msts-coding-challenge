class CreatePhones < ActiveRecord::Migration[5.1]
  def change
    create_table :phones do |t|
      t.string :number
      t.boolean :active
      t.string :phone_type
      t.references :user
      t.timestamps null: false
      t.timestamps
    end
  end
end
