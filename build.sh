#!/bin/sh
# This will run bash again in our development container, this time with the
# database online via Compose:
# building the application container with Ruby
docker-compose build

# adding the host files to /etc/hosts
#echo "\n127.0.0.1 api.msts.local" >> "/etc/hosts"
ETC_HOSTS=/etc/hosts
# DEFAULT IP FOR HOSTNAME
IP="127.0.0.1"
# Hostname to add/remove.
HOSTNAME=api.msts.local

HOSTS_LINE="$IP\t$HOSTNAME"
if [ -n "$(grep $HOSTNAME /etc/hosts)" ]
    then
        echo "$HOSTNAME already exists : $(grep $HOSTNAME $ETC_HOSTS)"
    else
        echo "Adding $HOSTNAME to your $ETC_HOSTS";
        sudo -- sh -c -e "echo '$HOSTS_LINE' >> /etc/hosts";

        if [ -n "$(grep $HOSTNAME /etc/hosts)" ]
            then
                echo "$HOSTNAME was added succesfully \n $(grep $HOSTNAME /etc/hosts)";
            else
                echo "Failed to Add $HOSTNAME, Try again!";
        fi
fi

docker-compose run --rm --service-ports api rspec

docker-compose run --rm --service-ports api rails db:create db:migrate db:seed