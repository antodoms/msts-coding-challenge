# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

## Setup (Getting Started for the first time)

Clone the repository
```bash
git clone https://gitlab.com/antodoms/msts-coding-challenge.git
```

From the root directory run the below commands to make the setup file executable.

```bash
cd msts-coding-challenge
chmod +x ./build.sh
chmod +x ./start.sh
```

Then run setup.sh file as sudo user to build the docker container for the first time and do initial migration.
```bash
sudo ./build.sh
```

The above command will also run the Rspec test scripts after building the container.


## Running the App

Run the below command to start the docker infrastructure.

```bash
sudo ./start.sh
```

After that Navigate to http://api.msts.local or run the below command in terminal

```bash
open http://api.msts.local
```

## Usage

List of API Endpoints

| REQUEST TYPE  |   REQUEST                                     |   USAGE                                           |
| ------------- | --------------------------------------------- | ------------------------------------------------- |
| GET           |   /                                           |   A page to verify the web application is running |
| GET           |   /api/v1/users/                              |   Get the list of all Users                       |
| POST          |   /api/v1/users/                              |   Create a new User                               |
| GET           |   /api/v1/users/:user_id/                     |   Get a user details                              |
| PATCH         |   /api/v1/users/:user_id/                     |   Update a user details                           |
| PUT           |   /api/v1/users/:user_id/                     |   Update a user details                           |
| DELETE        |   /api/v1/users/:user_id/                     |   Delete a user                                   |
| GET           |   /api/v1/users/:user_id/address/             |   Get all the addresses of a User                 |
| POST          |   /api/v1/users/:user_id/address/             |   Create a new address for a user                 |
| GET           |   /api/v1/users/:user_id/address/:address_id/ |   Get a particular address of a user              |
| PATCH         |   /api/v1/users/:user_id/address/:address_id/ |   Update a particular address of a user           |
| PUT           |   /api/v1/users/:user_id/address/:address_id/ |   Update a particular address of a user           |
| DELETE        |   /api/v1/users/:user_id/address/:address_id  |   Delete a particular address of a user           |
| GET           |   /api/v1/users/:user_id/phone/               |   Get a list of all the phones of a user          |
| POST          |   /api/v1/users/:user_id/phone/               |   Create a new phone for a user                   |
| GET           |   /api/v1/users/:user_id/phone/:phone_id/     |   Get a particular phone details of a user        |
| PATCH         |   /api/v1/users/:user_id/phone/:phone_id/     |   Update a particular phone detail of a user      |
| PUT           |   /api/v1/users/:user_id/phone/:phone_id/     |   Update a particular phone detail of a user      |
| DELETE        |   /api/v1/users/:user_id/phone/:phone_id/     |   Delete a particular phone detail of a user      |


### EXAMPLES

#### Creating a User [POST]
The below request will create a user.

/api/v1/users/

```json
{"users": {
	"name": "Test User",
	"email": "test@gmail.com"
	}
}
```

#### Updating a User [PUT] or [PATCH]
The below request will update the user in database index 1 with the below information:

/api/v1/users/1/

```json
{"users": {
	"name": "Test User",
	"email": "test@gmail.com"
	}
}
```

#### Creating an address for a user [POST]
The below request will create the address for user in index 1.

/api/v1/users/1/address/

```json
{
        "address": {
            "address_type": "billing",
            "address_1": "300 city rd",
            "address_2": null,
            "address_3": null,
            "suburb": "melbourne",
            "state": "vic",
            "postcode": "3000"
        }
}
```

#### Update an address of a user [PUT] or [PATCH]
The below request will update the address index 1 provided it belongs to user index 1.

/api/v1/users/1/address/1/

```json
{
        "address": {
            "address_type": "other",
            "address_1": "50 whiteman st",
            "address_2": null,
            "address_3": null,
            "suburb": "southbank",
            "state": "vic",
            "postcode": "3006"
        }
}
```

#### Creating a phone for a user [POST]
The below request will create a phone number for a user in index 1.

/api/v1/users/1/phone/
```json
{
    "phone": { 
        "number": "06776558788", 
        "active": "true", 
        "phone_type": "mobile" 
    }
}
```

#### Updating a phone of a user [PUT] or [PATCH]
The below request will create a phone number for a user in index 1.

/api/v1/users/1/phone/
```json
{
    "phone": { 
        "number": "000000000788", 
        "active": "true", 
        "phone_type": "home" 
    }
}
```
